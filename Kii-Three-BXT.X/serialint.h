/* 
 * File:   serialint.h
 * Author: Bruno
 *
 * Created on 03 September 2015, 16:03
 */

#ifndef SERIALINT_H
#define	SERIALINT_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "serial.h"
#include "crc.h"
#include "interrupts.h"
#include "hardware_profile.h"

extern uint8_t PendingDSPRead;

void PostExtDataToInt(uint8_t* ExtMessage);
void PostIntMessage(uint8_t* IntMessage);
void LoopIntByte(uint8_t Data);
void GetIntByte(uint8_t* Byte, uint16_t TimeOut, bool* Success);
uint8_t* CheckIntSerialIn(bool SimplifiedMessaging);
void RequestDSP(uint8_t Address,uint8_t Nbytes);
void SerialInt10ms(void);
void ResetIntTimer(void);
void FlushInt(void);

#endif	/* SERIALINT_H */

