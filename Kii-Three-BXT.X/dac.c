/*
 * File:   dac.c
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 */

#include <xc.h>
#include <stdint.h>
#include "dac.h"
#include "spi.h"
#include "hardware_profile.h"

void AK4482_write(uint8_t reg, uint8_t val)
{   
    SPI_CS_DAC = CHIP_SELECT;   
    WriteSPI((unsigned)(AK4482_WRITE|reg)); 
    WriteSPI(val); 
    SPI_CS_DAC = CHIP_UNSELECT; 
}

void InitDAC(void)
{      
    AK4482_write(DAC_CONTROL1,C1_DIF1); //reset and power down for now
    AK4482_write(DAC_CONTROL2,(DEFAULT_C2|C2_SMUTE));
    AK4482_write(DAC_CONTROL3,C3_SD); //long delay
}

void SetDACMute(uint8_t mute)
{
    if(mute)AK4482_write(DAC_CONTROL2,(DEFAULT_C2|C2_SMUTE));
    else AK4482_write(DAC_CONTROL2,DEFAULT_C2);
}

void SetDACEnable(uint8_t enable)
{
    if(enable)AK4482_write(DAC_CONTROL1,(C1_DIF1|C1_PW|C1_RSTN));
    else AK4482_write(DAC_CONTROL1,C1_DIF1);;
}