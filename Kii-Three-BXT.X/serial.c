/*
 * File:   onebitserial.c
 * Author: Bruno Putzeys and Bart van der Laan for Kii Audio
 * For Kii Three DSP module V0.0
 *
 */

#include "serial.h"

/*
 * Format of message from master to slave:
 *  Send Data
 *      <HW Address> <SW Address> <Numbytes> <Data> <CRC>
 *
 * Receiver states
 *      Idle.
 *      Waiting for HW Address byte
 *      Waiting for SW Address byte
 *      Waiting for Numbytes
 *      Waiting for <Numbytes> byte
 *      Waiting for CRC byte
 *      Check CRC and process data if valid, otherwise report error
 */

//volatile uint8_t SerialHWAddress,SerialSWAddress,SerialMsgLength;

uint8_t CRCMessage(uint8_t *SerialMessage)
{
    unsigned char i,l;
    uint8_t CRCreg=0xFF;

    l=(unsigned)SerialMessage[NdataByte]+DataByte;
    for(i=0;i<l;i++)
    {
        CRC(SerialMessage[i],&CRCreg);
    }
    return CRCreg;
}

