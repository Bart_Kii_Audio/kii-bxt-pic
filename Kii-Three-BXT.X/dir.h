/* 
 * File:   dir.h
 * Author: KiiBart
 *
 * Created on 12 maart 2018, 11:34
 */

#ifndef DIR_H
#define	DIR_H

#define CS_DELAYus_DIR  2 //for repeated writes or reads min tcsh = 1us according to datasheet

//DIR states
#define DIR_Reset       0
#define DIR_Init        1
#define DIR_Locked      2
#define DIR_Unlocked    3

#define CS8416_ADDR     0x20
#define CS8416_WRITE    CS8416_ADDR|0x00
#define CS8416_READ     CS8416_ADDR|0x01

#define CONTROL0        0x00
#define CONTROL1        0x01
#define CONTROL2        0x02
#define CONTROL3        0x03
#define CONTROL4        0x04
#define CONTROL5        0x05
#define CONTROL6        0x06
#define CONTROLC        0x0C

#define C0_TRUNC        0x04
#define C0_PDUR         0x08
#define C0_FSWCLK       0x40

#define C1_CHS          0x01
#define C1_RMCKF        0x02
#define C1_HOLD0        0x04
#define C1_HOLD1        0x08
#define C1_INT0         0x10
#define C1_INT1         0x20
#define C1_MUTESA0      0x40
#define C1_SWCLK        0x80

#define C2_GPO0SEL0     0x01
#define C2_GPO0SEL1     0x02
#define C2_GPO0SEL2     0x04
#define C2_GPO0SEL3     0x08
#define C2_EMPH_CNTL0   0x10
#define C2_EMPH_CNTL1   0x20
#define C2_EMPH_CNTL2   0x40
#define C2_DETCI        0x80

#define FIXED_LOW       0x00
#define AUDIO           C2_GPO0SEL0|C2_GPO0SEL3
#define RERR            C2_GPO0SEL0|C2_GPO0SEL2
#define NVERR           C2_GPO0SEL1|C2_GPO0SEL2
#define GPO_TX          C2_GPO0SEL0|C2_GPO0SEL1|C2_GPO0SEL3
#define FIXED_HIGH      C2_GPO0SEL2|C2_GPO0SEL3

#define C3_GPO2SEL0     0x01
#define C3_GPO2SEL1     0x02
#define C3_GPO2SEL2     0x04
#define C3_GPO2SEL3     0x08
#define C3_GPO1SEL0     0x10
#define C3_GPO1SEL1     0x20
#define C3_GPO1SEL2     0x40
#define C3_GPO1SEL3     0x80

#define C4_TXSEL0       0x01
#define C4_TXSEL1       0x02
#define C4_TXSEL2       0x04
#define C4_RXSEL0       0x08
#define C4_RXSEL1       0x10
#define C4_RXSEL2       0x20
#define C4_RXD          0x40
#define C4_RUN          0x80

#define C5_SOLRPOL      0x01
#define C5_SOSPOL       0x02
#define C5_SODEL        0x04
#define C5_SOJUST       0x08
#define C5_SORES0       0x10
#define C5_SORES1       0x20
#define C5_SOSF         0x40
#define C5_SOMS         0x80

#define C6_PARM         0x01
#define C6_BIPM         0x02
#define C6_CONFM        0x04
#define C6_VM           0x08
#define C6_UNLOCKM      0x10
#define C6_CCRCM        0x20
#define C6_QCRCM        0x40

#define CC_PAR          0x01
#define CC_BIP          0x02
#define CC_CONF         0x04
#define CC_V            0x08
#define CC_UNLOCK       0x10
#define CC_CCRC         0x20
#define CC_QCRC         0x40

void InitDIR(void);
uint8_t CheckDIRError(void);
void SetDIRLED(uint8_t led);

#endif	/* DIR_H */

