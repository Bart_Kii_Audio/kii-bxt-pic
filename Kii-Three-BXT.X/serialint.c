/*
 * File:   onebitserial.c
 * Author: Bruno Putzeys and Bart van der Laan for Kii Audio
 * For Kii Three DSP module V0.0
 *
 */

#include "serialint.h"

//uint8_t PendingDSPRead;

/*
 * Format of message from master to slave:
 *  Send Data
 *      <HW Address> <SW Address> <Numbytes> <Data> <CRC>
 *
 * Receiver states
 *      Idle.
 *      Waiting for HW Address byte
 *      Waiting for SW Address byte
 *      Waiting for Numbytes
 *      Waiting for <Numbytes> byte
 *      Waiting for CRC byte
 *      Check CRC and process data if valid, otherwise report error
 */

/*void PostIntMessage(uint8_t* IntMessage)
{
    unsigned char i,j;
    j = IntMessage[NdataByte]+4; //incl. CRC
    for(i=0;i<j;i++)
    {
        PostIntByte(IntMessage[i]);
    }
}

void ResetIntTimer(void)
{
    IntTimeOut=SerialTimeOut;
    IntReceiverTimedOut=0;
}

void RequestDSP(uint8_t Address,uint8_t Nbytes)
{
    uint8_t IntMessage[5];

    PendingDSPRead=Address;
    IntMessage[AddrByte]=1|RnW;
    IntMessage[CmdByte]=Address;
    IntMessage[NdataByte]=1;
    IntMessage[DataByte]=Nbytes;
    IntMessage[DataByte+1]=CRCMessage(&IntMessage);
    PostIntMessage(&IntMessage);
    ResetIntTimer();
}


void PostExtDataToInt(uint8_t* ExtMessage)
{
    uint8_t i;

    for(i = 0; i < ExtMessage[NdataByte]; i++)
    {
        PostIntByte(ExtMessage[i+DataByte]);
    }
}

void FlushInt(void)
{
    while(DataInInt())ReadInt();
}

void GetIntByte(uint8_t* Byte, uint16_t TimeOut, bool* Success)
{
    IntTimeOut=TimeOut;
    while(!DataInInt()&&(IntTimeOut!=0))
    {
        asm("CLRWDT");
    }
    if(DataInInt())
    {
        *Byte=ReadInt();
        *Success=1;
    }
    else
        *Success=0;
}

void LoopIntByte (uint8_t Byte)
{
    PostExtByte(Byte);  //loopback data for debug
}

uint8_t* CheckIntSerialIn(bool SimplifiedMessaging)
{
    uint8_t IntReceivedByte;
    uint8_t Check;
    
    static uint8_t IntMessage[MaxMsgLength],IntReceiveMessagePointer;
    static uint8_t IntReceiverState = RecIdle;

    ClearOERR();
    if(IntReceiverBreak)
    {
        IntReceiverState=RecIdle;
        IntReceiverBreak=0;
    }
    if(DataInInt())
    {
        IntReceivedByte=ReadInt();

        if(SimplifiedMessaging)
        {
            IntMessage[0] = IntReceivedByte;
            IntReceiverState = RecIdle;
            return IntMessage;
        }
        else
        {
            switch(IntReceiverState)
            {
                case RecIdle:

                    IntMessage[AddrByte] = IntReceivedByte;
                    IntReceiverState = RecAddressed;

                    break;

                case RecAddressed:

                    IntMessage[CmdByte] = IntReceivedByte;
                    IntReceiverState = RecRegister;

                    break;

                case RecRegister:

                    IntMessage[NdataByte] = IntReceivedByte;
                    if(IntReceivedByte <= MaxMsgLength)
                        IntReceiverState = RecReadyForData;
                    else
                        IntReceiverState = RecIdle;
                    IntReceiveMessagePointer = DataByte;

                    break;

                case RecReadyForData:

                    if(IntReceiveMessagePointer != (IntMessage[NdataByte]+3))
                    {
                        IntMessage[IntReceiveMessagePointer] = IntReceivedByte;
                        IntReceiveMessagePointer++;
                    }

                    if(IntReceiveMessagePointer == (IntMessage[NdataByte]+3))
                    {
                        IntReceiverState = RecCheckCRC;
                    }

                    break;

                case RecCheckCRC:

                    IntMessage[IntReceiveMessagePointer] = IntReceivedByte;
                    Check=CRCMessage(IntMessage);
                    if(IntMessage[IntReceiveMessagePointer] == Check)
                    {
                        //MessageValidity = MessageValid;
                        IntReceiverState = RecIdle;
                        if(IntMessage[CmdByte]!=PendingDSPRead)
                        {
                            ResetIntTimer();
                            return NULL;    //Void Message if not in reply to pending request, put in another timeout to allow DSP request queue to flush.
                        }
                        else          
                            return IntMessage;
                    }
                    else
                    {
                        //MessageValidity = MessageUnValid;
                        IntReceiverState = RecIdle;
                    }
                    break;

                default:

                    IntReceiverState = RecIdle;
                    break;
            }
        }
    }
    return NULL;
}

*/