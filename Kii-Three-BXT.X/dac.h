/* 
 * File:   dac.h
 * Author: KiiBart
 *
 * Created on 12 maart 2018, 11:34
 */

#ifndef DAC_H
#define	DAC_H

#define AK4482_ADDR     0x40
#define AK4482_WRITE    AK4482_ADDR|0x20

#define MUTE            0x01
#define UNMUTE          0x00

#define DAC_ENABLE      0x01
#define DAC_DISABLE     0x00

#define DAC_CONTROL1    0x00
#define DAC_CONTROL2    0x01
#define DAC_CONTROL3    0x02

#define C1_RSTN         0x01
#define C1_PW           0x02
#define C1_DIF0         0x04
#define C1_DIF1         0x08
#define C1_DIF2         0x10
#define C1_ACKS         0x80

#define C2_SMUTE        0x01
#define C2_DEM0         0x02
#define C2_DEM1         0x04
#define C2_DFS0         0x08
#define C2_DFS1         0x10
#define C2_SLOW         0x20
#define C2_DZFM         0x40
#define C2_DZFE         0x80

#define C3_SD           0x01

#define NORMAL_SPEED    0x00
#define DOUBLE_SPEED    C2_DFS0
#define QUAD_SPEED      C2_DFS1

#define DEFAULT_C2      DOUBLE_SPEED|C2_DEM0//|C2_SLOW

void InitDAC(void);
void SetDACMute(uint8_t mute);
void SetDACEnable(uint8_t enable);

#endif	/* DAC_H */

