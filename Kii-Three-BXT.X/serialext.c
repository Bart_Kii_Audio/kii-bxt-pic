/*
 * File:   onebitserial.c
 * Author: Bruno Putzeys and Bart van der Laan for Kii Audio
 * For Kii Three DSP module V0.0
 *
 */

#include "serialext.h"

/*
 * Format of message from master to slave:
 *  Send Data
 *      <HW Address> <SW Address> <Numbytes> <Data> <CRC>
 *
 * Receiver states
 *      Idle.
 *      Waiting for HW Address byte
 *      Waiting for SW Address byte
 *      Waiting for Numbytes
 *      Waiting for <Numbytes> byte
 *      Waiting for CRC byte
 *      Check CRC and process data if valid, otherwise report error
 */

//volatile uint8_t SerialHWAddress,SerialSWAddress,SerialMsgLength;

uint8_t TimeOut=0;

/*
void PostExtMessage(uint8_t* ExtMessage)
{
    unsigned char i,j;
    j = ExtMessage[NdataByte]+DataByte; //incl. CRC
    ExtMessage[j] = CRCMessage(ExtMessage);  //Recalculate the new CRC before passing on
    for(i=0;i<=j;i++)
    {
        PostExtByte(ExtMessage[i]);
    }
}

void QuickExtAck(uint8_t Addr, uint8_t Cmd)
{
    unsigned char AckMessage[4];
    AckMessage[AddrByte]= Addr&0xF0|0xA0;    //Set address bits to 0000, Set Reply bit, Set Ack bit
    AckMessage[CmdByte]=Cmd;
    AckMessage[NdataByte]=0;
    PostExtMessage(AckMessage);
}

void ExtAck(uint8_t* ExtMessage)
{
    unsigned char AckMessage[4];
    uint8_t AddressByte;
    AddressByte=ExtMessage[AddrByte];
    if((AddressByte&MessageForMask)==MessageForMe)        //Never ack broadcasts.
    {
        AckMessage[AddrByte]= AddressByte&0xF0|0xA0;    //Set address bits to 0000, Set Reply bit, Set Ack bit
        AckMessage[CmdByte]=ExtMessage[CmdByte];
        AckMessage[NdataByte]=0;
        PostExtMessage(AckMessage);
    }
}

void ExtNak(uint8_t* ExtMessage,uint8_t ErrCode)
{
    unsigned char AckMessage[4];
    AckMessage[AddrByte]=ExtMessage[AddrByte]&0xF0|0x80;    //Set address bits to 0000, Set Reply bit, Clear Ack bit
    AckMessage[CmdByte]=ExtMessage[CmdByte];
    AckMessage[NdataByte]=1;
    AckMessage[DataByte]=ErrCode;
    PostExtMessage(AckMessage);
}

uint8_t* CheckExtSerialIn(void)
{
    static uint8_t ExtMessage[MaxMsgLength],ExtReceiveMessagePointer;
    static uint8_t ExtReceiverState = RecIdle;
    uint8_t ExtReceivedByte;
    uint8_t Ndata;
    uint8_t ForWho;
    uint8_t CRCin;
    AddrByte_t AddressByte;

    if(ExtReceiverBreak)
    {
        ExtReceiverState=RecIdle;
        ExtReceiverBreak=0;
    }
    if(DataInExt())
    {
        TimeOut=SerialTimeOut;
        ExtReceivedByte=ReadExt();
        switch(ExtReceiverState)
        {
            case RecIdle:

                ExtMessage[AddrByte] = ExtReceivedByte;
                ExtReceiverState = RecAddressed;

                break;

            case RecAddressed:

                ExtMessage[CmdByte] = ExtReceivedByte;
                ExtReceiverState = RecRegister;

                break;

            case RecRegister:

                Ndata = ExtReceivedByte;
                ExtMessage[NdataByte] = Ndata;
                if(Ndata == 0)
                    ExtReceiverState = RecCheckCRC;
                else if(Ndata <= MaxMsgLength)
                    ExtReceiverState = RecReadyForData;
                else
                    ExtReceiverState = RecIdle;
                ExtReceiveMessagePointer = DataByte;

                break;

            case RecReadyForData:

                if(ExtReceiveMessagePointer != (Ndata+3))
                {
                    ExtMessage[ExtReceiveMessagePointer] = ExtReceivedByte;
                    ExtReceiveMessagePointer++;
                }

                if(ExtReceiveMessagePointer == (Ndata+3))
                {
                    ExtReceiverState = RecCheckCRC;
                }

                break;

            case RecCheckCRC:

                ExtMessage[ExtReceiveMessagePointer] = ExtReceivedByte;
                ExtReceiverState = RecIdle;
                CRCin=CRCMessage(ExtMessage);
                if(ExtMessage[ExtReceiveMessagePointer] == CRCin)
                {
                    AddressByte.BYTE=ExtMessage[AddrByte];
                    ForWho = AddressByte.BYTE&MessageForMask;

                    if(AddressByte.Reply)        //Pass reply messages by incrementing address.
                    {
                        ExtMessage[AddrByte]=AddressByte.BYTE+1;
                        PostExtMessage(ExtMessage);
                    }
                    else if(ForWho > MessageForMe) //Pass messages for others by decrementing address
                    {
                        ExtMessage[AddrByte]=AddressByte.BYTE-1; //subtract an address number
                        PostExtMessage(ExtMessage); //Forward the received message as it is for someone else
                    }
                    else        //for me or for all
                    {
                        if(ForWho == MessageForAll)
                        {
                            PostExtMessage(ExtMessage); //first send the complete received message on forward before we start to work with it
                        }
                        return ExtMessage;
                    }
                }
                else
                {
                    ExtNak(ExtMessage,CRCin);
                    //ExtMessage[AddrByte]=0x80;
                    //PostExtMessage(ExtMessage);
                }
                break;

            default:

                ExtReceiverState = RecIdle;
                break;
        }
    }
    else if(TimeOut==0)
    {
        ExtReceiverState = RecIdle;
    }
    return NULL;
}
*/

void SerialExt10ms(void)
{
    if(TimeOut!=0)
        TimeOut--;
}