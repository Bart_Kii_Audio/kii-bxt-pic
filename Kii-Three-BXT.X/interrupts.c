/*
 * File:   interrupts.c
 * Author: Bruno Putzeys and Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include <xc.h>

#include "hardware_profile.h"             /* XC8 General Include File */
#include "typedefs.h"
#include "serial.h"

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

volatile bit Tick = 0;
volatile bit IntReceiverBreak = 0;
volatile bit ExtReceiverBreak = 0;

volatile uint8_t ExtSerialOut[SerialBufSize];
volatile uint8_t ExtSerialIn[SerialBufSize];
volatile uint8_t ExtTXWI=0;
volatile uint8_t ExtTXRI=0;
volatile uint8_t ExtRXWI=0;
volatile uint8_t ExtRXRI=0;

volatile uint8_t IntSerialOut[SerialBufSize];
volatile uint8_t IntSerialIn[SerialBufSize];
volatile uint8_t IntTXWI=0;
volatile uint8_t IntTXRI=0;
volatile uint8_t IntRXWI=0;
volatile uint8_t IntRXRI=0;

volatile uint16_t PostScale=0;
volatile bit IntReceiverTimedOut = 0;
volatile uint16_t IntTimeOut=0;

volatile unsigned char trash;

void interrupt isr(void)
{
    if(C1IF && C1IE)
    {
        C1IF = 0;
        SMPS_OFF_SET = 1;
        TRIS_SMPS_OFF = 0;
        IO_NRST = 0;
        LED=0;
        //StoreCurrentSettings();
        while(1)LED=(unsigned)!LED;       
    }
    
    // Serial communication with the outside world
    if(TX1IF)
    {
        if (ExtTXRI==ExtTXWI)
            TX1IE=0;
        else
        {
            TX1REG=ExtSerialOut[ExtTXRI++];
            if (ExtTXRI>=SerialBufSize) ExtTXRI=0;
        }
    }
    if (RC1IF) //Note: As data gets transmitted, it gets read back and overwrites itself.
    {
        if(RC1STAbits.FERR) //re-sync receiver signal with break signal
        {
            ExtReceiverBreak=1;
            trash = RC1REG; //Read RCREG come hell or high water.
        }
        else
        {
            ExtSerialIn[ExtRXWI++]=RC1REG;     //Read RCREG come hell or high water.
            if(ExtRXWI>=SerialBufSize)ExtRXWI=0;
        }
    }

    // Serial communication with the connected Kii Three on top
    if(TX2IF)
    {
        if (IntTXRI==IntTXWI)
            TX2IE=0;
        else
        {
            TX2REG=IntSerialOut[IntTXRI++];
            if (IntTXRI>=SerialBufSize) IntTXRI=0;
        }
    }
    if(RC2IF) //Note: As data gets transmitted, it gets read back and overwrites itself.
    {        
        if(RC2STAbits.FERR) //re-sync receiver signal with break signal
        {
           IntReceiverBreak = 1;
           trash = RC2REG; //Read RCREG come hell or high water.
        }
        else
        {
            IntSerialIn[IntRXWI++]=RC2REG;     //Read RCREG come hell or high water.
            if(IntRXWI>=SerialBufSize)IntRXWI=0;
        }
    }
    
    // Timer stuff
    if(TMR2IF && TMR2IE)    
    {
        if(PostScale!=0)
            PostScale--;
        else
        {
            Tick=1;
            PostScale=9;
            if(IntTimeOut!=0)
                IntReceiverTimedOut|=(--IntTimeOut==0);
        }
        TMR2IF = 0;
    }
}

bool DataInExt(void)
{
    return(unsigned)(ExtRXRI!=ExtRXWI);
}

bool DataInInt(void)
{
    return(unsigned)(IntRXRI!=IntRXWI);
}

/*bool DataOutExt(void)
{
    return(unsigned)((TX1IE==1)||(!TX1STAbits.TRMT));
}*/

char ReadExt(void)
{
    char x=ExtSerialIn[ExtRXRI++];
    if(ExtRXRI>=SerialBufSize)ExtRXRI=0;
    return(x);
}

char ReadInt(void)
{
    char x=IntSerialIn[IntRXRI++];
    if(IntRXRI>=SerialBufSize)IntRXRI=0;
    return(x);
}

void PostExtByte(uint8_t Data)
{
    ExtSerialOut[ExtTXWI]=Data;
    ExtTXWI++;
    if (ExtTXWI >= SerialBufSize)
    {
        ExtTXWI=0;
    }
    TX1IE=1;
}

void PostIntByte(uint8_t Data)
{
    IntSerialOut[IntTXWI]=Data;
    IntTXWI++;
    if (IntTXWI >= SerialBufSize)
    {
        IntTXWI=0;
    }
    TX2IE=1;
}

/*void ClearOERR (void)
{
    if(RC1STAbits.OERR)
    {
        RCREG1;
        RCREG1;
        RC1STAbits.CREN=0;
        RC1STAbits.CREN=1;
    }
    if(RC2STAbits.OERR)
    {
        RCREG2;
        RCREG2;
        RC2STAbits.CREN=0;
        RC2STAbits.CREN=1;
    }
}*/

