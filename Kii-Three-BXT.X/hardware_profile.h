/*
 * File:   hardware_profile.h
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 */

#ifndef HARDWARE_PROFILE_H
#define	HARDWARE_PROFILE_H

#define _XTAL_FREQ 64000000

//Port declarations
//Outputs
#define IO_NRST                 LATCbits.LATC0
#define LED                     LATAbits.LATA1
#define SMPS_OFF_GET            PORTAbits.RA2

#define SERIAL_EXT_TX           PORTCbits.RC6
#define SERIAL_INT_TX           PORTBbits.RB5

#define AES_THRU_S              LATCbits.LATC2
#define AES_DIG_S               LATCbits.LATC1

#define SPI_DAT_O               LATBbits.LATB3
#define SPI_DAT_I               LATBbits.LATB2
#define SPI_CLK                 LATBbits.LATB1
#define SPI_CS_DIR              LATBbits.LATB0
#define SPI_CS_DAC              LATCbits.LATC5

//Inputs
#define SMPS_OFF_SET            LATAbits.LATA2

#define SERIAL_EXT_RX           PORTCbits.RC7
#define SERIAL_INT_RX           PORTBbits.RB4

#define PIN_I2C_SDA             PORTCbits.RC4
#define PIN_I2C_SCL             PORTCbits.RC3

#define PIN_CMP_12V             PORTAbits.RA0

//TRIS declarations
//Outputs
#define TRIS_IO_NRST            TRISCbits.TRISC0
#define TRIS_LED                TRISAbits.TRISA1

#define TRIS_SERIAL_EXT_TX      TRISCbits.TRISC6
#define TRIS_SERIAL_INT_TX      TRISBbits.TRISB5

#define TRIS_AES_THRU_S         TRISCbits.TRISC2
#define TRIS_AES_DIG_S          TRISCbits.TRISC1

#define TRIS_SPI_DAT_O          TRISBbits.TRISB3
#define TRIS_SPI_DAT_I          TRISBbits.TRISB2
#define TRIS_SPI_CLK            TRISBbits.TRISB1
#define TRIS_SPI_CS_DIR         TRISBbits.TRISB0
#define TRIS_SPI_CS_DAC         TRISCbits.TRISC5

//Inputs
#define TRIS_SMPS_OFF           TRISAbits.TRISA2

#define TRIS_SERIAL_EXT_RX      TRISCbits.TRISC7
#define TRIS_SERIAL_INT_RX      TRISBbits.TRISB4

#define TRIS_PIN_I2C_SDA        TRISCbits.TRISC4
#define TRIS_PIN_I2C_SCL        TRISCbits.TRISC3

#define TRIS_PIN_CMP_12V        TRISAbits.TRISA0

//General declarations
#define INPUT_PIN       1
#define OUTPUT_PIN      0

#define CHIP_UNSELECT   1
#define CHIP_SELECT     0

#define LED_OFF         0
#define LED_ON          1

#define AES_THRU_RJ45   0
#define AES_THRU_XLR    1

#define XLR_ANA         0
#define XLR_DIG         1

void InitPIC(void);
void SMPSOn(void);
void SMPSOff(void);

#endif	/* HARDWARE_PROFILE_H */
