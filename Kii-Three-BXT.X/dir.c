/*
 * File:   dir.c
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 */

#include <xc.h>
#include <stdint.h>
#include "dir.h"
#include "spi.h"
#include "hardware_profile.h"

void CS8416_write(uint8_t reg,uint8_t val)
{   
    SPI_CS_DIR = CHIP_SELECT;   
    WriteSPI(CS8416_WRITE); 
    WriteSPI(reg); 
    WriteSPI(val); 
    SPI_CS_DIR = CHIP_UNSELECT; 
    __delay_us(CS_DELAYus_DIR);
}

unsigned char CS8416_read(uint8_t reg)
{   
    unsigned char val;
    SPI_CS_DIR = CHIP_SELECT;   
    WriteSPI(CS8416_WRITE); 
    WriteSPI(reg); 
    SPI_CS_DIR = CHIP_UNSELECT;
    __delay_us(CS_DELAYus_DIR);
    SPI_CS_DIR = CHIP_SELECT; 
    WriteSPI(CS8416_READ);
    val=ReadSPI(); 
    SPI_CS_DIR = CHIP_UNSELECT; 
    __delay_us(CS_DELAYus_DIR);
    return val;
}

void InitDIR(void)
{    
    CS8416_write(CONTROL0,0x00);  
    CS8416_write(CONTROL1,(C1_SWCLK|C1_HOLD0)); 
    CS8416_write(CONTROL2,FIXED_LOW); 
    CS8416_write(CONTROL3,FIXED_LOW); 
    CS8416_write(CONTROL4,C4_RUN); 
    CS8416_write(CONTROL5,C5_SOMS); 
    CS8416_write(CONTROL6,(C6_PARM|C6_BIPM|C6_CONFM|C6_VM|C6_UNLOCKM|C6_CCRCM|C6_QCRCM));   
}

uint8_t CheckDIRError(void)
{
    if(CS8416_read(CONTROLC)!=0) //there was an error, sticky bits!
        return 1;
    else
        return 0;
}

void SetDIRLED(uint8_t led)
{
    if(led==LED_ON)CS8416_write(CONTROL2,FIXED_HIGH);
    else CS8416_write(CONTROL2,FIXED_LOW);
}