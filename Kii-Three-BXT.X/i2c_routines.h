/*
 * File:   i2c_routines.h
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 *
 */

#include <xc.h>
#include <stdint.h>
#include "typedefs.h"

#ifndef I2C_ROUTINES_H
#define	I2C_ROUTINES_H

/* SSPCON1 REGISTER */
#define   SSPENB    			0b00100000  	/* Enable serial port and configures SCK, SDO, SDI*/
#define   SLAVE_7   			0b00000110     	/* I2C Slave mode, 7-bit address*/
#define   SLAVE_10  			0b00000111    	/* I2C Slave mode, 10-bit address*/
#define   MASTER    			0b00001000     	/* I2C Master mode */
#define   MASTER_FIRMW			0b00001011		//I2C Firmware Controlled Master mode (slave Idle)
#define   SLAVE_7_STSP_INT 		0b00001110		//I2C Slave mode, 7-bit address with Start and Stop bit interrupts enabled
#define   SLAVE_10_STSP_INT 	0b00001111		//I2C Slave mode, 10-bit address with Start and Stop bit interrupts enabled

/* SSPSTAT REGISTER */
#define   SLEW_OFF  			0b10000000  	/* Slew rate disabled for 100kHz mode */
#define   SLEW_ON   			0b00000000  	/* Slew rate enabled for 400kHz mode  */

#define i2c_buffer_size 32

#define CloseI2C1()     SSP1CON1 &=0xDF
#define IdleI2C1()      while ((SSP1CON2 & 0x1F) | (SSP1STATbits.R_W))
#define StartI2C1()     SSP1CON2bits.SEN=1;while(SSP1CON2bits.SEN)
#define RestartI2C1()   SSP1CON2bits.RSEN=1;while(SSP1CON2bits.RSEN)
#define NotAckI2C1()    SSP1CON2bits.ACKDT=1, SSP1CON2bits.ACKEN=1;while(SSP1CON2bits.ACKEN)
#define WaitI2C1()      while ((SSP1STAT & 0x04) || (SSP1CON2 & 0x1F));

extern I2C_Errorbits_t i2c_error;

void InitI2C(void);
void I2C_Send(unsigned char addr, unsigned char reg, unsigned char data);
unsigned char I2C_Get(unsigned char addr, unsigned char reg);


#endif	/* I2C_ROUTINES_H */

