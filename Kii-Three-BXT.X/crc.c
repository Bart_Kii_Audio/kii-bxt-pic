/*
 * File:   crc.c
 * Author: Bruno
 *
 * Created on 31 August 2015, 19:46
 */

#include "crc.h"

void CRC(uint8_t Data,uint8_t *CRCreg)
{
    unsigned char j,b;
    for(j=8;j>0;j--)
    {
        b=(unsigned)Data&1;
        Data>>=1;
        if((*CRCreg&0x80)!=0)
            *CRCreg=(unsigned)((*CRCreg<<1)+b)^7;
        else
            *CRCreg=(unsigned)(*CRCreg<<1)+b;
    }
}

