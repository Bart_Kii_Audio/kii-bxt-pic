/* 
 * File:   crc.h
 * Author: Bruno
 *
 * Created on 31 August 2015, 19:46
 */

#ifndef CRC_H
#define	CRC_H

#include <stdint.h>

void CRC(uint8_t Data,uint8_t *CRCreg);

#endif	/* CRC_H */

