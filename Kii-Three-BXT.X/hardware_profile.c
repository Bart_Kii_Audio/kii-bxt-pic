/*
 * File:   hardware_profile.c
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three DSP module V0.0
 *
 */

// PIC18F26K40 Configuration Bit Settings
// 'C' source line config statements

// CONFIG1L
#pragma config FEXTOSC = HS     // External Oscillator mode Selection bits (HS (crystal oscillator) above 8 MHz; PFM set to high power)
#pragma config RSTOSC = HFINTOSC_64MHZ// Power-up default value for COSC bits (HFINTOSC with HFFRQ = 64 MHz and CDIV = 1:1)

// CONFIG1H
#pragma config CLKOUTEN = OFF   // Clock Out Enable bit (CLKOUT function is disabled)
#pragma config CSWEN = ON       // Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = ON       // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor enabled)

// CONFIG2L
#pragma config MCLRE = INTMCLR  // Master Clear Enable bit (If LVP = 0, MCLR pin function is port defined function; If LVP =1, RE3 pin fuction is MCLR)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (Power up timer enabled)
#pragma config LPBOREN = ON     // Low-power BOR enable bit (ULPBOR enabled)
#pragma config BOREN = SBORDIS  // Brown-out Reset Enable bits (Brown-out Reset enabled , SBOREN bit is ignored)

// CONFIG2H
#pragma config BORV = VBOR_285  // Brown Out Reset Voltage selection bits (Brown-out Reset Voltage (VBOR) set to 2.85V)
#pragma config ZCD = OFF        // ZCD Disable bit (ZCD disabled. ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
#pragma config PPS1WAY = ON     // PPSLOCK bit One-Way Set Enable bit (PPSLOCK bit can be cleared and set only once; PPS registers remain locked after one clear/set cycle)
#pragma config STVREN = ON      // Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config DEBUG = OFF      // Debugger Enable bit (Background debugger disabled)
#pragma config XINST = OFF      // Extended Instruction Set Enable bit (Extended Instruction Set and Indexed Addressing Mode disabled)

// CONFIG3L
#pragma config WDTCPS = WDTCPS_31// WDT Period Select bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE = ON        // WDT operating mode (WDT enabled regardless of sleep)

// CONFIG3H
#pragma config WDTCWS = WDTCWS_7// WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS = SC      // WDT input clock selector (Software Control)

// CONFIG4L
#pragma config WRT0 = OFF       // Write Protection Block 0 (Block 0 (000800-003FFFh) not write-protected)
#pragma config WRT1 = OFF       // Write Protection Block 1 (Block 1 (004000-007FFFh) not write-protected)
#pragma config WRT2 = OFF       // Write Protection Block 2 (Block 2 (008000-00BFFFh) not write-protected)
#pragma config WRT3 = OFF       // Write Protection Block 3 (Block 3 (00C000-00FFFFh) not write-protected)

// CONFIG4H
#pragma config WRTC = OFF       // Configuration Register Write Protection bit (Configuration registers (300000-30000Bh) not write-protected)
#pragma config WRTB = OFF       // Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF       // Data EEPROM Write Protection bit (Data EEPROM not write-protected)
#pragma config SCANE = ON       // Scanner Enable bit (Scanner module is available for use, SCANMD bit can control the module)
#pragma config LVP = OFF        // Low Voltage Programming Enable bit (HV on MCLR/VPP must be used for programming)

// CONFIG5L
#pragma config CP = OFF         // UserNVM Program Memory Code Protection bit (UserNVM code protection disabled)
#pragma config CPD = OFF        // DataNVM Memory Code Protection bit (DataNVM code protection disabled)

// CONFIG5H

// CONFIG6L
#pragma config EBTR0 = OFF      // Table Read Protection Block 0 (Block 0 (000800-003FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR1 = OFF      // Table Read Protection Block 1 (Block 1 (004000-007FFFh) not protected from table reads executed in other blocks)
#pragma config EBTR2 = OFF      // Table Read Protection Block 2 (Block 2 (008000-00BFFFh) not protected from table reads executed in other blocks)
#pragma config EBTR3 = OFF      // Table Read Protection Block 3 (Block 3 (00C000-00FFFFh) not protected from table reads executed in other blocks)

// CONFIG6H
#pragma config EBTRB = OFF      // Boot Block Table Read Protection bit (Boot Block (000000-0007FFh) not protected from table reads executed in other blocks)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>            /* XC8 General Include File */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "hardware_profile.h"

void InitPIC(void)
{   
    TRIS_LED                = OUTPUT_PIN;
    
    TRIS_SERIAL_EXT_TX      = OUTPUT_PIN;
    TRIS_SERIAL_INT_TX      = OUTPUT_PIN;

    TRIS_AES_THRU_S         = OUTPUT_PIN;
    TRIS_AES_DIG_S          = OUTPUT_PIN;

    TRIS_SPI_DAT_O          = OUTPUT_PIN;
    TRIS_SPI_DAT_I          = INPUT_PIN;
    TRIS_SPI_CLK            = OUTPUT_PIN;
    TRIS_SPI_CS_DIR         = OUTPUT_PIN;
    TRIS_SPI_CS_DAC         = OUTPUT_PIN;
    
    TRIS_SERIAL_EXT_RX      = INPUT_PIN;
    TRIS_SERIAL_INT_RX      = INPUT_PIN;

    TRIS_PIN_I2C_SDA        = INPUT_PIN;
    TRIS_PIN_I2C_SCL        = INPUT_PIN;

    TRIS_SMPS_OFF           = INPUT_PIN;
    TRIS_IO_NRST            = OUTPUT_PIN;
    
    TRIS_PIN_CMP_12V        = INPUT_PIN;
    
    //Analog input disabling
    ANSELA = 0;
    ANSELB = 0;
    ANSELC = 0;
      
    IO_NRST = 0;
    LED = 1;
    AES_THRU_S = AES_THRU_RJ45;
    AES_DIG_S = XLR_ANA;
            
    while(!OSCSTATbits.HFOR) {}
    
    //DAC voltage reference
    DAC1CON0     = 0b10000000; //VDD as reference to gnd
    DAC1CON1     = 13; //Vout = VDD * DACR<4:0>/2^5
    
    //Comparator, CM1NCH default connected to RA0 or C1IN0-
    CM1PCH       = 0b00000101; //DAC output for positive reference
    CM1CON0      = 0x80; //Comparator 1 Enabled
    CM1CON1      = 0x02; //C1IF on positive edge    
            
    //Timer 2. One tick is 1ms = 125kHz*125 cycles -> of fosc/4 - 8 times prescale - 16 times postscale.
    T2CON        = 0b10111111;
    T2CLKCON     = 0b00000001; //Fosc/4
    PR2          = 125;

    //Serial Ports
    SP1BRG          = 25; //baud (38400) = 64MHz / (64*(1+b))
    SP1BRGH         = 0;
    TX1STAbits.SYNC = 0;
    RCSTA1bits.SPEN = 1;
    TX1IE           = 0; //block transmit interrupt
    TX1STAbits.TXEN = 1;
    RCSTA1bits.CREN = 1; //Turn on receiver
    RC1IE           = 1; //Allow receive interrupt

    RC6PPS = 0x09;          //EUSART1 TX
    RX1PPS = 0b00010111;    //EUSART1 RX
    
    SP2BRG          = 25; //baud (38400) = 64MHz / (16*(1+b))
    SP2BRGH         = 0; 
    TX2STAbits.SYNC = 0;
    RCSTA2bits.SPEN = 1;
    TX2IE           = 0; //block transmit interrupt
    TX2STAbits.TXEN = 1;
    RCSTA2bits.CREN = 1; //Turn on receiver 
    RC2IE           = 1; //Allow receive interrupt
    
    RB5PPS = 0x0B;          //EUSART2 TX
    RX2PPS = 0b00001100;    //EUSART2 RX

    // I2C ports
    SSP1STAT = 0b10000000;
    SSP1CON1 = 0b00111000;
    SSP1CON2 = 0b00000000;
    SSP1CON3 = 0b00000000;
    SSP1ADD = 159;  //100kHz

    // SPI ports
    SPI_CS_DIR = CHIP_UNSELECT;
    SPI_CS_DAC = CHIP_UNSELECT;
    
    RB1PPS = 0x11;  //SCK MSSP2
    RB3PPS = 0x12;  //SDO MSSP2
  
    //Interrupts
    TMR2IE       = 1; //Allow timer 2
    PEIE         = 1; //Allow peripheral interrupts
    C1IE         = 0; //Disallow Comparator 1 interrupt
    GIE          = 1; //Allow global interrupts 
}

void SMPSOn(void)
{
    TRIS_SMPS_OFF = 1;
}

void SMPSOff(void)
{
    SMPS_OFF_SET = 1;
    TRIS_SMPS_OFF = 0;
}