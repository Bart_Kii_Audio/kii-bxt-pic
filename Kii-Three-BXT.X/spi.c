/*
 * File:   spi.c
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 */

#include <xc.h>
#include <stdint.h>
#include "spi.h"

void OpenSPI(unsigned char sync_mode, unsigned char bus_mode, uint8_t smp_phase)
{
    SSP2STAT &= 0x3F;               // power on state 
    SSP2CON1 = 0x00;                // power on state
    SSP2CON1 |= sync_mode;          // select serial mode 
    SSP2STAT |= smp_phase;          // select data input sample phase

    switch( bus_mode )
    {
        case 0:                       // SPI2 bus mode 0,0
            SSP2STATbits.CKE = 1;       // data transmitted on rising edge
            break;    
        case 2:                       // SPI2 bus mode 1,0
            SSP2STATbits.CKE = 1;       // data transmitted on falling edge
            SSP2CON1bits.CKP = 1;       // clock idle state high
            break;
        case 3:                       // SPI2 bus mode 1,1
            SSP2CON1bits.CKP = 1;       // clock idle state high
            break;
        default:                      // default SPI2 bus mode 0,1
            break;
    }
    SSP2CON1bits.SSPEN2 = 1; // enable synchronous serial port 
}

signed char WriteSPI(unsigned char data_out)
{
    unsigned char TempVar;  
     
    TempVar = SSP2BUF;      // Clears BF
    SSP2IF = 0;             // Clear interrupt flag
    SSP2BUF = data_out;     // write byte to SSP2BUF register
    if (SSP2CON1 & 0x80)    // test if write collision occurred
        return ( -1 );      // if WCOL bit is set return negative #
    else
        while(!SSP2IF);     // wait until bus cycle complete
    return (0);             // if WCOL bit is not set return non-negative#
}

unsigned char ReadSPI()
{
    unsigned char TempVar;
       
    TempVar = SSP2BUF;      // Clear BF
    SSP2IF = 0;             // Clear interrupt flag
    SSP2BUF = 0x00;         // initiate bus cycle
    while(!SSP2IF);         // wait until cycle complete
    return (SSP2BUF);       // return with byte read 
}

void InitSPI(void)
{
    OpenSPI(SPI_FOSC_64,MODE_00,SMPMID);
}