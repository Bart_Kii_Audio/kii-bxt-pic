/* 
 * File:   serialext.h
 * Author: Bruno
 *
 * Created on 03 September 2015, 16:29
 */

#ifndef SERIALEXT_H
#define	SERIALEXT_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "serial.h"
#include "crc.h"
#include "interrupts.h"
#include "hardware_profile.h"

void PostExtMessage(uint8_t* ExtMessage);
void QuickExtAck(uint8_t Addr, uint8_t Cmd);
void ExtAck(uint8_t* ExtMessage);
void ExtNak(uint8_t* ExtMessage,uint8_t ErrCode);
uint8_t* CheckExtSerialIn(void);
void SerialExt10ms(void);

#endif	/* SERIALEXT_H */

