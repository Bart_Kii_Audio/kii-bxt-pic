/*
 * File:   serial.h
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 *
 */

#ifndef SERIAL_H
#define	SERIAL_H

#include <stdint.h>
#include <stdio.h>
#include "crc.h"
#include "hardware_profile.h"
#include "typedefs.h"

#define RnW                 0x40

#define MessageUnValid      0x00
#define MessageValid        0x01

#define MessageForMask      0x0F
#define MessageForAll       0x00
#define MessageForMe        0x01

#define AddrByte            0x00
#define CmdByte             0x01
#define NdataByte           0x02
#define DataByte            0x03

#define SerialBufSize       80
#define MaxMsgLength        68

//Receiver states
#define RecIdle             0x00
#define RecAddressed        0x01
#define RecRegister         0x02
#define RecReadyForData     0x03
#define RecCheckCRC         0x04
#define RecNewMessage       0x05

#define SerialTimeOut       100     //units of 10ms
#define UploadTimeOut       300

uint8_t CRCMessage(uint8_t *SerialMessage);

#endif	/* SERIAL_H */

