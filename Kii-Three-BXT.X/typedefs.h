/*
 * File:   typedefs.h
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 *
 */

#ifndef TYPEDEFS_H
#define	TYPEDEFS_H

// bitfield definitions

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    Product         :4;
        unsigned    Reserved        :1;
        unsigned    NoCoefs         :1;
        unsigned    NoDSPCode       :1;
        unsigned    NoPICCode       :1;
    };
} DeviceID_t;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    Minor           :4;
        unsigned    Major           :4;
    };
} Version_t;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    On              :1;
        unsigned    Auto            :1;
        unsigned    Error           :1;
        unsigned    Reserved        :5;
    };
} PowerStatus_t;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    DigitalChannel  :2;
        unsigned    Reserved        :6;
    };
} Channel_t;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    Polarity        :1;
        unsigned    Reserved        :7;
    };
} Specials2_t;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    DigitalSource   :2;
        unsigned    Analogue        :1;
        unsigned    Reserved        :5;
    };
} Source_t;

#define DigitalXLR                  0
#define DigitalCAT5                 1
#define DigitalWISA                 2
#define DigitalAuto                 3

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    Present         :1;
        unsigned    Active          :1;
    };
};

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    Mute            :1;
        unsigned    Zoom            :1;
        unsigned    Sim             :1;
        unsigned    SmallOrVented   :1;
        unsigned    LowLatency      :1;
        unsigned    TestMode        :1;
        unsigned    ShowLimiters    :1;
        unsigned    BlinkLEDs       :1;
    };
} SpecialModes_t;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned    InputChannel    :1;
        unsigned    Contour         :1;
        unsigned    Boundary        :1;
        unsigned    Source          :1;
        unsigned    Reserved        :4;
    };
} Overrides_t;

typedef union
{
    uint8_t BYTES[];
    struct
    {
        DeviceID_t DeviceID;
        Version_t HardwareVersion;
        Version_t PICAppVersion;
        Version_t PICBootVersion;
        Version_t DSPAppVersion;
        Version_t DSPBootVersion;
        Version_t CoefFormatVersion;
        uint8_t Manufacture;
        uint24_t ProductSerial;
        uint24_t TweetSerial;
        uint24_t MidSerial;
        uint24_t OperatingTime;
        uint8_t CRCErrorCount;

        uint8_t Reserved1[11];

        PowerStatus_t PowerStatus;
        uint8_t Volume;
        Source_t Source;
        SpecialModes_t SpecialModes;
        Overrides_t Overrides;
        uint8_t Delay;
        uint8_t LowShelfCorner;
        uint8_t LowShelfGain;
        uint8_t HighShelfCorner;
        uint8_t HighShelfGain;
        uint8_t BoundaryGain;
        uint8_t LEDBright;
        Channel_t Channel;
        uint8_t LipSynch;
        Specials2_t Specials2;
        uint8_t AnaSens;
        uint8_t SineGenFreq;
    };
} SpeakerSettings_t;

//Indices to the above.

#define DeviceID_i          0x00
#define HardwareVersion_i   0x01
#define PICAppVersion_i     0x02
#define PICBootVersion_i    0x03
#define DSPAppVersion_i     0x04
#define DSPBootVersion_i    0x05
#define CoefFormatVersion_i 0x06
#define Manufacture_i       0x07
#define ProductSerial_i     0x08
#define TweetSerial_i       0x0B
#define MidSerial_i         0x0E
#define LastProtectedLoc    0x10
#define OperatingTime_i     0x11
#define CRCErrorCount_i     0x14

#define PowerStatus_i       0x20
#define Volume_i            0x21
#define Source_i            0x22
#define SpecialModes_i      0x23
#define Overrides_i         0x24
#define Delay_i             0x25
#define LowShelfCorner_i    0x26
#define LowShelfGain_i      0x27
#define HighShelfCorner_i   0x28
#define HighShelfGain_i     0x29
#define BoundaryGain_i      0x2A
#define LEDBright_i         0x2B
#define Channel_i           0x2C
#define LipSynch_i          0x2D
#define Specials2_i         0x2E
#define AnaSens_i           0x2F
#define SineWaveFreq_i      0x30
#define LastReg             0x30

//These are virtual registers. Writing or reading does not advance address
#define Amp1Status_i        0x50
#define Amp2Status_i        0x51
#define DSPTemperature_i    0x52
#define ModulePresence_i    0x53

#define EnableSerialWrite   0x54
#define DisableSerialWrite  0x55
#define ForceCommitChanges  0x56
#define BootloadCmdStart1   0x5A
#define BootloadCmdStart2   0x5B
#define BootloadCmdMessage  0x5C
#define BootloadCmdBlessing 0x5D
#define UploadCmdStart1     0x60
#define UploadCmdStart2     0x61
#define UploadCmdLength     0x62
#define UploadCmdOffset     0x63
#define UploadCmdData       0x64
#define UploadCmdBlessing   0x65
#define InputSamplingRate_i 0x66

typedef union
{
    unsigned char BYTES[2];
    struct
    {
        unsigned    PowerStatus     :1;
        unsigned    Volume          :1;
        unsigned    Channel         :1;
        unsigned    SpecialModes    :1;
        unsigned    Delay           :1;
        unsigned    Contour         :1;
        unsigned    Boundary        :1;
        unsigned    Source          :1;
        unsigned    AnaSens         :1;
        unsigned    SineGenFreq     :1;
        unsigned                    :6;
    };
} Changes_t;

typedef union
{
    unsigned char BYTES[];
    struct
    {
        unsigned char ErrorStatusReg0;
        unsigned char ErrorStatusReg1;
        unsigned char EnableReg;
        unsigned char PlusHV;
        unsigned char MinHV;
        unsigned char VDR;
        signed char   Temperature;                                                //changed to signed for negative values
        unsigned char Fsw;
        unsigned char ProductCode;
    };
    struct
    {   //ErrorStatusReg0
        struct
        {
            unsigned DCError        :1;
            unsigned PlusHVOver     :1;
            unsigned MinHVOver      :1;
            unsigned PlusHVUnder    :1;
            unsigned MinHVUnder     :1;
            unsigned OverloadError  :1;
            unsigned FreqError      :1;
            unsigned AmpFail        :1;
        };
        //ErrorStatusReg1
        struct
        {
            unsigned VDROver        :1;
            unsigned VDRUnder       :1;
            unsigned AmpReady       :1;
            unsigned OverTemp       :1;
            unsigned FeedbackError  :1;
            unsigned VopError       :1;
            unsigned B1b6           :1;
            unsigned B1b7           :1;
        };
        // EnableReg
        struct
        {
            unsigned AmpEnable      :1;
            unsigned                :7;
        };
    };
} AMP_I2CMem_t;

typedef union {
    uint8_t MuxRegister2;
    struct {
        unsigned low_nibble :4;
        unsigned high_nibble :4;
    };
    struct {
        unsigned ROTA_1     :1;
        unsigned ROTA_2     :1;
        unsigned ROTA_4     :1;
        unsigned ROTA_8     :1;
        unsigned ROTB_1     :1;
        unsigned ROTB_2     :1;
        unsigned ROTB_4     :1;
        unsigned ROTB_8     :1;
    };
} MuxRegister2bits_t;

typedef union {
    uint8_t MuxRegister1;
    struct {
        unsigned EMPTY_1    :1;
        unsigned EMPTY_2    :1;
        unsigned EMPTY_3    :1;
        unsigned EMPTY_4    :1;
        unsigned SW_B       :1;
        unsigned SW_A       :1;
        unsigned SW_PUSH    :1;
        unsigned EMPTY_5    :1;
    };
} MuxRegister1bits_t;

typedef union {
    uint8_t I2C_Error;
    struct {
        unsigned BUS_COLL    :1;
        unsigned NO_ACK      :1;
    };
} I2C_Errorbits_t;


typedef union {
    unsigned int WORD;
    struct {
        unsigned low_byte :8;
        unsigned high_byte :8;
    };
    struct {
        unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
    };
} WORD_t;


typedef union
{
    uint32_t DWORD;
    struct
    {
        WORD_t  LSW;
        WORD_t  MSW;
    };
} DWORD_t;


typedef union {
    unsigned char BYTE;
    struct {
        unsigned b0 :1;
        unsigned b1 :1;
        unsigned b2 :1;
        unsigned b3 :1;
        unsigned b4 :1;
        unsigned b5 :1;
        unsigned b6 :1;
        unsigned b7 :1;
    };
} BYTE_t;

typedef union {
    unsigned char BYTE;
    struct {
        unsigned a0     :1;
        unsigned a1     :1;
        unsigned a2     :1;
        unsigned a3     :1;
        unsigned        :1;
        unsigned Ack    :1;
        unsigned RnW    :1;
        unsigned Reply  :1;
    };
} AddrByte_t;

typedef union {
    long LW;
    unsigned long uLW;
    int W[2];
    unsigned int uW[2];
    char B[4];
    unsigned char uB[4];
    float FLT;
} UniVar;

typedef union
{
    uint8_t BYTE;
    struct
    {
        unsigned PowerLimitInd :1;
        unsigned TweetLimit :1;
        unsigned MidLimit   :1;
        unsigned WoofLimit  :1;
        unsigned Excursion  :1;
        unsigned            :3;
    };
} LimiterState_t;

typedef union
{
    uint32_t DWORD;
    uint8_t BYTES[4];
    struct
    {
        unsigned NOAUDIOL       :1;
        unsigned NOAUDIOR       :1;
        unsigned NOAUDIOLR      :1;
        unsigned VALID          :1;
        unsigned LOCK           :1;
        unsigned NOSTREAM       :1;
        unsigned PARITYERROR    :1;
        unsigned BIPHASEERROR   :1;
        unsigned Reserved1      :8;
        unsigned Reserved2      :8;
        unsigned Reserved3      :8;
    };
} DirStat_t;

#define ThermalMask     0b00001111

#endif	/* TYPEDEFS_H */
