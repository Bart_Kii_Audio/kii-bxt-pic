/* 
 * File:   main.h
 * Author: Bruno
 *
 * Created on 03 September 2015, 19:04
 */

#ifndef MAIN_H
#define	MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>        /* For true/false definition */

#include <xc.h>

#include "hardware_profile.h"
#include "typedefs.h"
#include "serial.h"
#include "serialint.h"
#include "serialext.h"
#include "i2c_routines.h"
#include "spi.h"
#include "dir.h"
#include "dac.h"
//#include "amplifiers.h"
#include "interrupts.h"
//#include "pgmwrite.h"

//Defaults
#define DeviceID_v              0x03
#define HWV                     0x10
#define SWV                     0x27

#define UploadStateIdle         0x00
#define UploadStateStart1       0x01
#define UploadStateStart2       0x02
#define UploadStateLength       0x03
#define UploadStateOffset       0x04

#define JustPluggedIn           0
#define Initialized             1
#define Standby                 2
#define PoweringUp              3
#define Active                  4
#define PoweringDown            5
#define Uploading               6
#define PreparingToPeriscope    7
#define Periscoping             8
#define ErrorState              9

#define DebounceCounts          10
#define ChangeTime              1000    //10 seconds
#define LatencyToggleTime       ChangeTime-250     //2.5 seconds
#define SleepMins               15

//Timing
#define PlugInTime              15
#define RestartTime1            10
#define RestartTime2            5
#define LedTime                 50  //0.5 seconds
#define SleepTime               15  //Minutes
#define KeepAwakeTime           3   //Minutes
#define PeriscopeLength         1   //s
#define PeriscopeCycle          5   //s

//void StoreCurrentSettings(void);

#endif	/* MAIN_H */

