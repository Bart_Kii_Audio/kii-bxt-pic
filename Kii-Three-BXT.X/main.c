/*
 * File:   main.c
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 *
 */

#include "main.h"

SpeakerSettings_t SpeakerSettings;

volatile uint8_t IntReceiverState,ExtReceiverState;

/*uint8_t UploadState = UploadStateIdle;
uint8_t BootloadState = UploadStateIdle;
Changes_t Changes;
SpecialModes_t OldSpecialModes,AppliedSpecialModes;
SpecialModes_t SpecialModesChanges;
bit ProtectSerials;
*/
uint8_t SpeakerState;
uint8_t SleepAfter;         //Minutes.
uint8_t OperateTimer;
uint8_t LedTimer;
uint8_t DIR_State;

/*bit ResetRequested = 0;
bit LatencyToggleRequested = 0;
bit StandbyToggleRequested = 0;
bit StandbyTogglePossible = 0;


const uint8_t Defaults[LastReg+1] =
{
    DeviceID_v, HWV, SWV, 0x10, 0x20, 0x10, 0x10, 0x18,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x02, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x64, 0x80,
    0xB9, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 
    0x00
};

void GetStoredSettings(void)
{
    char i;
    
    for (i=0;i<=LastReg;i++)
        SpeakerSettings.BYTES[i]=EEread(i);
    if(SpeakerSettings.PowerStatus.Auto)        //Compiler bug: SpeakerSettings.PowerStatus.On=!SpeakerSettings.PowerStatus.Auto is compiled incorrectly
        SpeakerSettings.PowerStatus.On=0;
    else
        SpeakerSettings.PowerStatus.On=1;
    SpeakerSettings.PowerStatus.Error=0;    //Clear error on new powerup.
    SpeakerSettings.SpecialModes.Mute=0;
    SpeakerSettings.SpecialModes.Sim=0;
    SpeakerSettings.SpecialModes.TestMode=0;
    SpeakerSettings.SpecialModes.BlinkLEDs=0;
    SpeakerSettings.DeviceID.NoCoefs=!CoefUploadOK();
    SpeakerSettings.DeviceID.NoDSPCode=!DSPUploadOK();
}

void StoreCurrentSettings(void)
{
    char i,c;
    for (i=0;i<=LastReg;i++)
    {
        c=SpeakerSettings.BYTES[i];
        if ( (c!=EEread(i)) && ((i>LastProtectedLoc) || !ProtectSerials))
            LED0=0;
            EEwrite(i,c);
            LED0=1;
    }
}

void InstallFactoryDefaults(void)
{
    char i;
    for (i=0;i<=LastReg;i++)
        SpeakerSettings.BYTES[i]=Defaults[i];
    ProtectSerials=0;
    StoreCurrentSettings();
    ProtectSerials=1;
}

void RestoreFactoryDefaults(void)
{
    char i;
    for (i=PowerStatus_i;i<=LastReg;i++)
        SpeakerSettings.BYTES[i]=Defaults[i];
    SpeakerSettings.PowerStatus.On=1;
    StoreCurrentSettings();
    Changes.BYTES[0]=0xff;
    Changes.BYTES[1]=0xff;
    SpecialModesChanges.BYTE=0xff;
}

void AmpOn(void)
{
    I2C_Send(0xB0,0x02,0x01);
    I2C_Send(0xB2,0x02,0x01);
}

void AmpOff(void)
{
    I2C_Send(0xB0,0x02,0x00);
    I2C_Send(0xB2,0x02,0x00);
}

void ApplySettings(void)
{
    SpecialModes_t NewSpecialModes;
    static uint8_t DepopState=0;
    static bit ApplySpecialModeChanges=0;

    if(Changes.Boundary)
    {
        SendBoundaryToDSP(SpeakerSettings.BoundaryGain);
        Changes.Boundary=0;
    }
    if(Changes.Contour)
    {
        SendContourToDSP(SpeakerSettings.LowShelfCorner,SpeakerSettings.LowShelfGain,SpeakerSettings.HighShelfCorner,SpeakerSettings.HighShelfGain);
        Changes.Contour=0;
    }
    if(Changes.Channel)
    {
        SendChannelToDSP(SpeakerSettings.Channel);
        Changes.Channel=0;
        Changes.Volume=1;
    }
    if(Changes.Volume)
    {
        SendVolumeToDSP(SpeakerSettings.Volume,(SpeakerSettings.Specials2.Polarity==1));
        Changes.Volume=0;
    }
    if(Changes.AnaSens)
    {
        SendAnaSensToDSP(SpeakerSettings.AnaSens);
        Changes.AnaSens=0;
    }
    if(Changes.SineGenFreq)
    {
        SendSineGenFreqToDSP(SpeakerSettings.SineGenFreq);
        Changes.SineGenFreq=0;
    }
    if(Changes.Source)
    {
        SendSourceToDSP(SpeakerSettings.Source);
        Changes.Source=0;
    }
    if(Changes.Delay)
    {
        SendDelayToDSP(SpeakerSettings.LipSynch,SpeakerSettings.Delay);
        Changes.Delay=0;
    }
    if(Changes.PowerStatus)
    {
        Changes.PowerStatus=0;
    }

    //Special modes. Some need depopping
    ApplySpecialModeChanges=0;
    NewSpecialModes.BYTE=AppliedSpecialModes.BYTE;

    if(SpecialModesChanges.SmallOrVented && !SpeakerSettings.SpecialModes.Sim)      //Sim not on, just apply SmallOrVented.
    {
        NewSpecialModes.SmallOrVented=SpeakerSettings.SpecialModes.SmallOrVented;
        ApplySpecialModeChanges=1;
        SpecialModesChanges.SmallOrVented=0;
    }
    if(SpecialModesChanges.LowLatency || SpecialModesChanges.Sim || (SpecialModesChanges.SmallOrVented && SpeakerSettings.SpecialModes.Sim))
    {
        switch(DepopState)
        {
            case 0:
                if(!AppliedSpecialModes.Mute)                   //If not muted, mute and wait
                {
                    NewSpecialModes.Mute=1;
                    ApplySpecialModeChanges=1;
                    PopTimer=30;
                }
                DepopState=1;
                break;                
            case 1:
                if(PopTimer<10)                                 //Well and truly muted
                {
                    NewSpecialModes.LowLatency=SpeakerSettings.SpecialModes.LowLatency;
                    NewSpecialModes.Sim=SpeakerSettings.SpecialModes.Sim;
                    NewSpecialModes.SmallOrVented=SpeakerSettings.SpecialModes.SmallOrVented;
                    ApplySpecialModeChanges=1;
                    DepopState=2;
                }            
                break;
            case 2:
                if(PopTimer==0)                                 //Pop wait time over
                {
                    SpecialModesChanges.LowLatency=0;
                    SpecialModesChanges.Sim=0;
                    SpecialModesChanges.SmallOrVented=0;
                    NewSpecialModes.Mute=SpeakerSettings.SpecialModes.Mute;
                    ApplySpecialModeChanges=1;
                    DepopState=0;
                }
                break;
            default:
                DepopState=0;
        }
    }
    if(SpecialModesChanges.Mute)
    {
        if(SpeakerSettings.SpecialModes.Mute)PopTimer=20;            //Make note of mute in case one of the popping commands followed on rapidly.
        NewSpecialModes.Mute=SpeakerSettings.SpecialModes.Mute;
        ApplySpecialModeChanges=1;
        SpecialModesChanges.Mute=0;
    }

    NewSpecialModes.ShowLimiters=SpeakerSettings.SpecialModes.ShowLimiters;
    NewSpecialModes.TestMode=SpeakerSettings.SpecialModes.TestMode;
    NewSpecialModes.Zoom=SpeakerSettings.SpecialModes.Zoom;
    if((AppliedSpecialModes.BYTE!=NewSpecialModes.BYTE)||ApplySpecialModeChanges)
    {
        SendSpecialModeToDSP(NewSpecialModes);
        AppliedSpecialModes.BYTE=NewSpecialModes.BYTE;
    }
}
*/
void InitVariables (void)
{
    SpeakerState = JustPluggedIn;
    OperateTimer = PlugInTime;
    LedTimer = LedTime;
    //ProtectSerials=1;
    //GetStoredSettings();
    //UploadState = UploadStateIdle;
    //BootloadState = UploadStateIdle;
    //ReadSwitches(0);                        //Read switch settings but do not interpret changes as turning off override.
    //OldSpecialModes.BYTE = SpeakerSettings.SpecialModes.BYTE;
    //AppliedSpecialModes.BYTE=0;
    //SpecialModesChanges.BYTE=0xFF;
    //Changes.BYTES[0]=0xFF;
    //Changes.BYTES[1]=0xFF;
    //LimiterState.BYTE=0;
    //Periscope=0;
}
/*
void Migrate(void)
{
    if(EEread(DeviceID_i)!=DeviceID_v)          //Happens only for a fresh unit
        InstallFactoryDefaults();
    if(EEread(HardwareVersion_i)!=HWV)          //Happens only for a fresh unit
        InstallFactoryDefaults();
    if(EEread(PICAppVersion_i)<0x20)            //Coming from v1.0, treat as fresh.
        InstallFactoryDefaults();
    if(EEread(PICAppVersion_i)!=SWV)
        EEwrite(PICAppVersion_i,SWV);
}

void HandleReset(void)
{
    StopDSPComms();
    if(!DSPCommsRunning)
    {
        RestoreFactoryDefaults();
        StartDSPComms();
        ResetRequested=0;
    }
}

void ToggleLatency(void)
{
    uint8_t Msg[5];
    Msg[AddrByte]=0;
    Msg[CmdByte]=SpecialModes_i;
    Msg[NdataByte]=1;
    SpeakerSettings.SpecialModes.LowLatency=!SpeakerSettings.SpecialModes.LowLatency;
    SpecialModesChanges.LowLatency=1;
    Msg[DataByte]=SpeakerSettings.SpecialModes.BYTE;
    PostExtMessage(Msg);
    LatencyToggleRequested=0;
}

void ToggleStandby(void)
{
    SpeakerSettings.PowerStatus.Auto=!SpeakerSettings.PowerStatus.Auto;
    StandbyToggleRequested = 0;
}

void SendPower(void)
{
    uint8_t Msg[5];
    Msg[AddrByte]=0;
    Msg[CmdByte]=PowerStatus_i;
    Msg[NdataByte]=1;
    Msg[DataByte]=SpeakerSettings.PowerStatus.BYTE;
    PostExtMessage(Msg);
}
*/

void CheckDIR(void)
{
    if(CheckDIRError()!=0)
    {
        if(DIR_State == DIR_Init || DIR_State == DIR_Locked)
        {
            DIR_State = DIR_Unlocked;
            SetDACMute(MUTE);
            SetDACEnable(DAC_DISABLE);           
            SetDIRLED(LED_OFF);
        }
    }
    else
    {
        if(DIR_State == DIR_Init || DIR_State == DIR_Unlocked)
        {
            DIR_State = DIR_Locked;
            SetDACEnable(DAC_ENABLE);
            SetDACMute(UNMUTE);
            SetDIRLED(LED_ON);
        }
    }    
}

void Operate (void)
{
    asm("CLRWDT"); //clear the watchdog timer while normal operation continues  
    
 /*   ReadSwitches(1);

    OperateLEDs();
    
    if(ResetRequested)
        HandleReset();
    if(LatencyToggleRequested)
        ToggleLatency();
    if(StandbyToggleRequested)
        ToggleStandby();

    SetSource(SpeakerSettings.Source);      //Bug fix to insure switch from digital to analogue is spotted during standby mode.
*/
    switch(SpeakerState)
    {
        case JustPluggedIn:
            SMPSOff();          
            IO_NRST=0;
            DIR_State = DIR_Reset;
            if(OperateTimer<=RestartTime2)
            {
                //AmpOff();
                SpeakerState=Initialized;
            }
            break;
        case Initialized:
            if(OperateTimer==0)
            {
                SpeakerState=Standby;
                C1IF=0;
                C1IE=1;
            }
            break;
        case Standby:
            IO_NRST=0;
            DIR_State = DIR_Reset;
            //if(DSPUploadOK()&&CoefUploadOK())
            //{
                if(OperateTimer==0)
                {
                    //if(SpeakerSettings.PowerStatus.On)
                    //{
                        //DSPOn();
                        SMPSOn();
                        //AmpOn();        //Will take a while to turn on anyway
                        OperateTimer=RestartTime1;
                        SpeakerState=PoweringUp;
                    //}
                    //else if(Periscope)
                    //{
                        //DSPOn();
                        //OperateTimer=RestartTime1;
                        //SpeakerState=PreparingToPeriscope;
                    //}
                }
            //}
            //else
                //SpeakerState=Uploading;
            break;
        /*case PreparingToPeriscope:
            if(OperateTimer<=RestartTime2)
            {
                //DSP_NRST=0;
                BootDSPNormal();
                SpeakerState=Periscoping;
            }
            break;
        case Periscoping:
            if(!Periscope)
            {
                DSP_NRST=1;
                DSPOff();
                OperateTimer=RestartTime2;
                SpeakerState=Standby;
            }
            break;*/
        case PoweringUp:
            if(OperateTimer<=RestartTime2)
            {
                //DSP_NRST=0;
                //BootDSPNormal();
                IO_NRST=1;
                __delay_us(10); //before starting to write to the CS8416
                if(DIR_State == DIR_Reset)
                {
                    InitDIR();
                    DIR_State = DIR_Init;
                }
                InitDAC();
                SpeakerState=Active;
                
                //Changes.BYTES[0]=0xFF;
                //Changes.BYTES[1]=0xFF;
                //SpecialModesChanges.BYTE=0xFF;
            }
            break;
        case Active:
            CheckDIR();
            /*
            if(DSPState==BootedNormal)
            {
                ApplySettings();
            }
            if(!SpeakerSettings.PowerStatus.On)
            {
                AmpOff();
                OperateTimer=RestartTime2;
                SpeakerState=PoweringDown;
            }
            if(SMPS_OFF_GET)
            {
                AmpOff();
                DSPOff();
                SpeakerState=ErrorState;
            }*/
            break;
        case PoweringDown:
            if(OperateTimer==0)
            {
                IO_NRST=0;
                DIR_State = DIR_Reset;
                SMPSOff();
                OperateTimer=RestartTime2;
                SpeakerState=Standby;
            }
            break;
        case Uploading:
            SMPSOff();
            break;
        //case Error or Uploading do nothing
    } 
}
/*

void HandleExtWrite(uint8_t* ExtMessage)
{
    uint8_t i;
    uint8_t Command;
    bool Success;
    uint8_t Reply;
    uint8_t Check;
    static DWORD_t UploadLength;
    static uint32_t UploadBytesDone;
    static bit CoefsNotCode;

    Command = ExtMessage[CmdByte];
    if(Command<=LastReg)        //Real Register, allows consecutive writes in one go
    {
        ExtAck(ExtMessage);
        for(i=0;i<ExtMessage[NdataByte];i++)
        {
            Command = ExtMessage[CmdByte]+i;
            if((Command<=LastReg) && ((Command>LastProtectedLoc) || !ProtectSerials))
                SpeakerSettings.BYTES[Command]=ExtMessage[DataByte+i];

            switch(Command)
            {
                case PowerStatus_i:
                    Changes.PowerStatus=1;
                    if(SpeakerSettings.PowerStatus.On)
                    {
                        SleepAfter=SleepTime;
                        Periscope=0;
                    }
                    break;
                case Volume_i:
                    Changes.Volume=1;
                    break;
                case AnaSens_i:
                    Changes.AnaSens=1;
                    break;                    
                case Channel_i:
                    Changes.Channel=1;
                    SpeakerSettings.Overrides.InputChannel=1;
                    break;
                case Source_i:
                    Changes.Source=1;
                    SpeakerSettings.Overrides.Source=1;
                    break;
                case SpecialModes_i:
                    SpecialModesChanges.BYTE=OldSpecialModes.BYTE^SpeakerSettings.SpecialModes.BYTE;
                    OldSpecialModes.BYTE=SpeakerSettings.SpecialModes.BYTE;
                case Specials2_i:
                    Changes.Volume=1;
                    break;
                case Overrides_i:
                    Changes.Boundary=1;
                    Changes.Channel=1;
                    Changes.Contour=1;
                    break;
                case Delay_i:
                    Changes.Delay=1;
                    break;
                case LowShelfCorner_i:
                case LowShelfGain_i:
                case HighShelfCorner_i:
                case HighShelfGain_i:
                    Changes.Contour=1;
                    SpeakerSettings.Overrides.Contour=1;
                    break;
                case BoundaryGain_i:
                    Changes.Boundary=1;
                    SpeakerSettings.Overrides.Boundary=1;
                    break;
                case LipSynch_i:
                    Changes.Delay=1;
                    break;
            }
        }
    }
    else //Virtual Registers, allows multibyte writes to one "register"
    {
        switch(Command)
        {
            case UploadCmdStart1:
                UploadState = UploadStateStart1;
                ExtAck(ExtMessage);
                break;

            case UploadCmdStart2:
                if(UploadState == UploadStateStart1)
                {
                    if(StartDSPUpload())
                    {
                        UploadState = UploadStateStart2;
                        ExtAck(ExtMessage);
                    }
                    else
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x80);
                    }
                }
                else
                {
                    UploadState = UploadStateIdle;
                    ExtNak(ExtMessage,UploadState);
                }
                break;
            case UploadCmdLength:
                if(UploadState == UploadStateStart2)
                {
                    Success=0;
                    if(((ExtMessage[DataByte]&0x3F)==0)&&(ExtMessage[NdataByte]==4))
                    {
                        UploadLength.LSW.low_byte=ExtMessage[DataByte];
                        UploadLength.LSW.high_byte=ExtMessage[DataByte+1];
                        UploadLength.MSW.low_byte=ExtMessage[DataByte+2];
                        UploadLength.MSW.high_byte=ExtMessage[DataByte+3];
                        UploadBytesDone=0;
                        PostExtDataToInt(ExtMessage);
                        GetIntByte(&Reply,UploadTimeOut,&Success);
                    }
                    if(!Success)
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x81);
                    }
                    else if(Reply!=3)
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x82);
                    }
                    else
                    {
                        UploadState = UploadStateLength;
                        ExtAck(ExtMessage);
                    }
                }
                else
                    ExtNak(ExtMessage,UploadState);
                break;
            case UploadCmdOffset:
                if(UploadState == UploadStateLength)
                {
                    Success=0;
                    if((ExtMessage[DataByte]|ExtMessage[DataByte+1]==0)&&ExtMessage[NdataByte]==4)
                    {
                        PostExtDataToInt(ExtMessage);
                        CoefsNotCode=(ExtMessage[DataByte+2]==0x02);
                        GetIntByte(&Reply,UploadTimeOut,&Success);
                        if(CoefsNotCode)
                            EEwrite(CoefUploadOKLoc,0);
                        else
                            EEwrite(DSPUploadOKLoc,0);
                    }
                    if(!Success)
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x83);
                    }
                    else if(Reply!=4)
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x84);
                    }
                    else
                    {
                        UploadState = UploadStateOffset;
                        ExtAck(ExtMessage);
                    }
                }
                else
                    ExtNak(ExtMessage,UploadState);
                break;
            case UploadCmdData:
                Check=0;
                Success=0;
                if(UploadState == UploadStateOffset)
                {
                    if(ExtMessage[NdataByte]==64)
                    {
                        for(i=0;i<64;i++)Check+=ExtMessage[DataByte+i];
                        PostExtDataToInt(ExtMessage);
                        UploadBytesDone+=64;
                        GetIntByte(&Reply,UploadTimeOut,&Success);
                    }
                    if(!Success)
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x85);
                    }
                    else if(Reply!=Check)
                    {
                        UploadState = UploadStateIdle;
                        ExtNak(ExtMessage,0x86);
                    }
                    else
                    {
                        ExtAck(ExtMessage);
                    }
                }
                break;

            case UploadCmdBlessing:
                if((UploadState==UploadStateIdle)&&(ExtMessage[DataByte]==0x02))
                {
                    EEwrite(CoefUploadOKLoc,1);
                    SpeakerSettings.DeviceID.NoCoefs=0;
                    SpeakerState=Standby;
                    ExtAck(ExtMessage);
                }
                else if((UploadState==UploadStateIdle)&&(ExtMessage[DataByte]==0x01))
                {
                    EEwrite(DSPUploadOKLoc,1);
                    SpeakerSettings.DeviceID.NoDSPCode=0;
                    SpeakerState=Standby;
                    ExtAck(ExtMessage);
                }
                else if(UploadBytesDone==UploadLength.DWORD)
                {
                    UploadState = UploadStateIdle;
                    if(CoefsNotCode)
                    {
                        EEwrite(CoefUploadOKLoc,1);
                        SpeakerSettings.DeviceID.NoCoefs=0;
                    }
                    else
                    {
                        EEwrite(DSPUploadOKLoc,1);
                        SpeakerSettings.DeviceID.NoDSPCode=0;
                    }
                    SpeakerState=Standby;       //This forces a normal startup cycle.
                    ExtAck(ExtMessage);
                }
                else
                    ExtNak(ExtMessage,0x87);
                //wait for the DSP to boot and load the normal program

                break;
            case BootloadCmdStart1:
                BootloadState = UploadStateStart1;
                ExtAck(ExtMessage);
                break;

            case BootloadCmdStart2:
                if(BootloadState == UploadStateStart1)
                {
                    BootloadState = UploadStateStart2;
                    EEwrite(UploadOKLoc,0);
                    ExtAck(ExtMessage);
                    while(DataOutExt());
                    asm("reset");
                }
                else
                {
                    ExtNak(ExtMessage,BootloadState);
                }
                break;
            case EnableSerialWrite:
                ProtectSerials=0;
                ExtAck(ExtMessage);
                break;
            case DisableSerialWrite:
                ProtectSerials=1;
                ExtAck(ExtMessage);
                break;
            case ForceCommitChanges:
                StoreCurrentSettings();
                ExtAck(ExtMessage);
                break;
            default:
                break;
        }
        ExtReceiverState = RecIdle; //Message processed so it is safe to receive a new one
    }
}

void HandleExtRead(uint8_t* ExtMessage)
{
    uint8_t NumToRead;
    uint8_t RegToRead;
    uint8_t i;
    UniVar tmp;

    SpeakerSettings.DSPAppVersion.BYTE=DSPAppVersion;
    SpeakerSettings.DSPBootVersion.BYTE=DSPBootVersion;

    RegToRead=ExtMessage[CmdByte];
    if(ExtMessage[NdataByte]==0)
        NumToRead=1;
    else
        NumToRead=ExtMessage[DataByte];
    ExtMessage[NdataByte]=NumToRead;
    for(i=0;i<NumToRead;i++)ExtMessage[DataByte+i]=0;
    switch(RegToRead)
    {
        case Amp1Status_i:
            for(i=0;i<NumToRead;i++)
                ExtMessage[DataByte+i]=I2C_Get(0xB0,i);
            break;
        case Amp2Status_i:
            for(i=0;i<NumToRead;i++)
                ExtMessage[DataByte+i]=I2C_Get(0xB2,i);
            break;
        case InputSamplingRate_i:
            tmp.FLT=SamplingRate;
            for(i=0;i<NumToRead;i++)
                ExtMessage[DataByte+i]=tmp.B[i];
            break;
        default:
            for(i=0;i<NumToRead;i++)
                ExtMessage[DataByte+i]=SpeakerSettings.BYTES[i+RegToRead];
    }
    ExtMessage[AddrByte]=0xA0;
    PostExtMessage(ExtMessage);
}

void HandleExtSerialCmd(uint8_t* ExtMessage)
{
    BYTE_t AddressByte;
    
    AddressByte.BYTE= ExtMessage[AddrByte];

    if(ExtMessage) //The CRC check passed so it is safe to process and pass on the message
    {
        if(AddressByte.b6)
            HandleExtRead(ExtMessage);
        else
            HandleExtWrite(ExtMessage);
    }   
}

void SleepWake(void)
{
    static bit PrevDigitalPresent=0;
    static bit NewDigitalDetected;

    NewDigitalDetected=!PrevDigitalPresent;
    NewDigitalDetected&=DigitalIsPresent;
    NewDigitalDetected&=(SelectedDigitalInput==XLRInput);
    PrevDigitalPresent=DigitalIsPresent;
    //send an on command downstream when turning on when within 3 minutes of turning off.
    if(SignalIsActive||NewDigitalDetected)     //if signal is present, or valid AES has arrived (with or without signal), turn on.
    {  
        
        if(SpeakerSettings.PowerStatus.Auto)
        {
            if((SleepAfter<KeepAwakeTime)||(!SpeakerSettings.PowerStatus.On))
            {
                SpeakerSettings.PowerStatus.On=1;
                Periscope=0;
                SendPower();
            }
        }
        SleepAfter=SleepTime;
        LED0 = ~LED0;
    }
    if((SleepAfter==0)&&SpeakerSettings.PowerStatus.Auto)SpeakerSettings.PowerStatus.On=0;
    
    SignalIsActive=0;
}*/

void EveryMinute(void)
{
    if(SpeakerSettings.PowerStatus.On)SpeakerSettings.OperatingTime++;
    if(SleepAfter!=0)SleepAfter--;    //commented for debug
}

void EverySecond(void)
{
    static uint8_t Seconds=0;
    //static uint8_t PeriscopeTimer=0;

    //if(SleepAfter!=0)SleepAfter--;      //uncommented for debug

    LED = (unsigned)~LED; //Toggle LED to see of the firmware is running properly
   
    //SleepWake();
    Seconds++;
    if(Seconds==60)
    {
        EveryMinute();
        Seconds=0;
    }
    /*if(SpeakerSettings.PowerStatus.Auto)
    {
        PeriscopeTimer++;
        if(PeriscopeTimer>=PeriscopeCycle)
        {
            PeriscopeTimer=0;
            Periscope=1;
        }
        if(PeriscopeTimer>(PeriscopeLength+1))
        {
            Periscope=0;
        }
    }
    else
    {
        Periscope=0;
        PeriscopeTimer=0;
    }*/
}

void TimerFarm(void)
{
    static uint8_t CentiSeconds=100;

    if(Tick)
    {
        if(CentiSeconds--==0)
        {
            CentiSeconds=100;
            EverySecond();
        }
        
        if(OperateTimer!=0)
            OperateTimer--;
        
        if(LedTimer!=0)
            LedTimer--;

        SerialExt10ms();
        
        Tick=0;
    }
}

int main()
{
    InitPIC();
    InitI2C();
    InitSPI();
    
    //Migrate();
    InitVariables();

    while(1)
    {
        TimerFarm();
        Operate(); 
        if(DataInExt())
        {
            PostIntByte(ReadExt());
        }
        if(DataInInt())
        {
            PostExtByte(ReadInt());
        }       
    }
}