/* 
 * File:   interrupts.h
 * Author: Bruno
 *
 * Created on 12 July 2015, 12:35
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#include <stdbool.h>
#include <stdint.h>

extern volatile bit IntReceiverBreak;
extern volatile bit ExtReceiverBreak;
extern volatile bit Tick;
extern volatile bit IntReceiverTimedOut;
extern volatile uint16_t IntTimeOut;

void PostExtByte(uint8_t Data);
void PostIntByte(uint8_t Data);
bool DataInExt(void);
bool DataInInt(void);
char ReadExt(void);
char ReadInt(void);
void ClearOERR (void);
bool DataOutExt(void);

#endif	/* INTERRUPTS_H */

