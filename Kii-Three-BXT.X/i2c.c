/*
 * File:   i2c.c
 * Author: Bart van der Laan for Kii Audio
 * For Kii Three BXT module V0.0
 */

#include <xc.h>
#include <stdint.h>
#include "i2c_routines.h"

I2C_Errorbits_t i2c_error = {0};
volatile unsigned char tmp;
signed char status;

void OpenI2C1( unsigned char sync_mode, unsigned char slew )
{
    SSP1STAT &= 0x3F;                // power on state 
    SSP1CON1 = 0x00;                 // power on state
    SSP1CON2 = 0x00;                 // power on state
    SSP1CON1 |= sync_mode;           // select serial mode 
    SSP1STAT |= slew;                // slew rate on/off   

    SSP1CON1 |= SSP1CON1bits.SSPEN1; // enable synchronous serial port 
}

unsigned char ReadI2C1( void )
{
    if( ((SSP1CON1&0x0F)==0x08) || ((SSP1CON1&0x0F)==0x0B) )	//master mode only
    {
        SSP1CON2bits.RCEN = 1;           // enable master for 1 byte reception
    }
    while ( !SSP1STATbits.BF );      // wait until byte received  
    return ( SSP1BUF );              // return with read byte 
}

signed char WriteI2C1(unsigned char data_out)
{
    SSP1BUF = data_out;           // write single byte to SSP1BUF
    if ( SSP1CON1bits.WCOL )      // test if write collision occurred
    {
        return (-1);              // if WCOL bit is set return negative #
    }
	else if( ((SSP1CON1&0x0F)==0x08) || ((SSP1CON1&0x0F)==0x0B) )	//master mode only	
    {
        while( SSP1STATbits.BF );   // wait until write cycle is complete      
        IdleI2C1();                  // ensure module is idle
        if ( SSP1CON2bits.ACKSTAT ) // test for ACK condition received
        {
             return ( -2 );				//Return NACK	
        }
        else 
        {
            return ( 0 );               //Return ACK
        }
    }
    return ( 3 ); //none of the above
}

void InitI2C (void)
{
    CloseI2C1(); //close i2c if was operating earlier

    OpenI2C1(MASTER,SLEW_OFF); //Master @100kHz
    SSP1ADD=0x9F; //100kHz Baud clock @64MHz
    //check for bus idle condition in multi master communication
    IdleI2C1();
}

void I2C_Send (unsigned char addr, unsigned char reg, unsigned char data)
{
    unsigned char iterate = 0;

    StartI2C1();
    i2c_error.I2C_Error = 0;

    tmp = SSP1BUF; //read any previous stored content in buffer to clear buffer full status
    do //write the address of the device for communication
    {
        status = WriteI2C1((unsigned)(addr | 0x00)); //write the address of slave, R/W bit is '0' to represent write mode
        if(status == -1) //check if bus collision happened
        {
            tmp = SSP1BUF; //upon bus collision detection clear the buffer,
            SSP1CON1bits.WCOL=0; // clear the bus collision status bit
            i2c_error.BUS_COLL = 1;
            iterate++;
        }
        else if(status == -2) //check if we did not receice an ack
        {
            i2c_error.NO_ACK = 1;
            iterate++;
        }
    }
    while(status!=0 && iterate<10); //write untill successful communication or iteration maximum

    if(status == 0)
    {
        status = WriteI2C1(reg);
        status = WriteI2C1(data);
    }
    IdleI2C1();
}

unsigned char I2C_Get (unsigned char addr, unsigned char reg)
{
    unsigned char iterate = 0;

    StartI2C1();
    i2c_error.I2C_Error = 0;

    tmp = SSP1BUF; //read any previous stored content in buffer to clear buffer full status
    do //write the address of the device for communication
    {
        status = WriteI2C1((unsigned)(addr | 0x00)); //write the address of slave, R/W bit is '0' to represent write mode
        if(status == -1) //check if bus collision happened
        {
            tmp = SSP1BUF; //upon bus collision detection clear the buffer,
            SSP1CON1bits.WCOL=0; // clear the bus collision status bit
            i2c_error.BUS_COLL = 1;
            iterate++;
        }
        else if(status == -2) //check if we did not receice an ack
        {
            i2c_error.NO_ACK = 1;
            iterate++;
        }
    }
    while(status!=0 && iterate<10); //write untill successful communication or iteration maximum

    if(status == 0)
    {
        status = WriteI2C1(reg);
    }
    IdleI2C1();
    RestartI2C1();
    IdleI2C1();

    tmp = SSP1BUF; //read any previous stored content in buffer to clear buffer full status
    do //write the address of the device for communication
    {
        status = WriteI2C1((unsigned)(addr | 0x01)); //write the address of slave, , R/W bit is '1' to represent read mode
        if(status == -1) //check if bus collision happened
        {
            tmp = SSP1BUF; //upon bus collision detection clear the buffer,
            SSP1CON1bits.WCOL=0; // clear the bus collision status bit
            i2c_error.BUS_COLL = 1;
            iterate++;
        }
    }
    while(status!=0 && iterate<10); //write untill successful communication
    tmp = 0;
    if(status == 0)
    {
        //*** Recieve data from slave ***
        tmp = ReadI2C1();
        NotAckI2C1(); //send the end of transmission signal through nack
        while( SSP1CON2bits.ACKEN!=0); //wait till ack sequence is complete
    }
    return tmp;
}

void Close_I2C (void)
{
    CloseI2C1(); //close I2C module
}